YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "OrgNetworkParser"
    ],
    "modules": [
        "organizations.controller"
    ],
    "allModules": [
        {
            "displayName": "organizations.controller",
            "name": "organizations.controller",
            "description": "Provides classes and methods that process requests for /api/organizations"
        }
    ],
    "elements": []
} };
});